#include <string>
#include <map>
#include <stack>
#include <exception>
#include <gdbm.h>

#ifndef SPREADSHEET_H
#define SPREADSHEET_H

const std::string PASSWORD_KEY = "CS__password";

struct Edit {
    std::string cell;
    std::string previous_value;
};

class no_more_undo: public std::exception
{
    virtual const char* what() const throw() {
        return "No more undos";
    }
};

/* Represents a spreadsheet, holding cell data
 * and allows for edit and undo operations. */
class Spreadsheet {
    private:
        /* Keep a database connection open for this spreadsheet */
        GDBM_FILE db;

        /* setter and getter for database key value pairs */
        void set_value(std::string key, std::string value);
        std::string get_value(std::string key);
        void delete_value(std::string key);

        /* Maintain a stack of changes, so we can undo them */
        std::stack<Edit> edits;

        /* maintain a version number for the spreadsheet, that is incremented with any changes */
        int version;

    public:
        /* Cell setters and accessors */
        void set_cell(std::string cell, std::string value);
        std::string get_cell(std::string cell);

        /* Version incrementer and getter */
        void increment_version();
        int get_version() const;

        /* passwords */
        void set_password(std::string new_password);
        bool passwords_match(std::string check_password);

        /* Undo, returns the name of the cell that was changed.
         * Returns 'END' if there are no more edits to undo */
        std::string undo();

        std::string get_xml();

        void clear_undo_stack();

        Spreadsheet(std::string name); // constructor
        ~Spreadsheet(); // destructor
};

#endif
