#include "Server.h"

/* Parser code */
#include "Parser.h"
Client* client_fetcher() {
    return (Client*) new Parser;
}
// */

/* Chat Client example
#include "ChatLineClient.h"
Client* client_fetcher() {
    return (Client*) new ChatLineClient;
}
// */

int main() {
    /* create a new server on the localhost */

    Server server("0.0.0.0", "1984");

    /* run the server with the echoclient */
    try {
        server.run(client_fetcher);
    } catch (std::exception) {
        return -1;
    }

    return 0;
}
