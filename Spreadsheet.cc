#include <cstdlib>
#include <string>
#include <sstream>
#include <map>
#include <stack>
#include <iostream>
#include <cstdio>
#include <cstring>
#include "Spreadsheet.h"

extern "C" {
#include <gdbm.h>
}

#define DATABASE "spreadsheets.db"

using namespace std;

/* Simply print the error if one occurs */
#if GDBM_VERSION_MINOR < 10
void __gdbm_fatal_printer() {
#else
void __gdbm_fatal_printer(const char *) {
#endif
    fprintf(stderr, "GDBM: %s.\n", gdbm_strerror(gdbm_errno));
}

/* escape the XML characters in the string */
void escape_xml(string& value) {
    size_t pos = 0, found = 0;
    int found_len = 3, i = 0;
    size_t findings[found_len];
    while (pos < value.size()) {
        printf("Found illegal.\n");
        /* Stupidly complicated MIN operator */
        findings[0] = value.find("<", pos);
        findings[1] = value.find(">", pos);
        findings[2] = value.find("&", pos);
        found = findings[0];
        for (i = 0; i < found_len; i++) {
            if (findings[i] < found) {
                found = findings[i];
            }
        }

        /* no occurences found */
        if (found == string::npos) { break; }

        if (value[found] == '<') {
            value.erase(found, 1);
            /* set the value as the escape */
            value.insert(found, "&lt;");
        } else if (value[found] == '>') {
            value.erase(found, 1);
            /* set the value as the escape */
            value.insert(found, "&gt;");
        } else if (value[found] == '&') {
            value.erase(found, 1);
            /* set the value as the escape */
            value.insert(found, "&amp;");
        }

        pos = found + 1;
    }
}

/* Cell setters and accessors */
void Spreadsheet::set_cell(std::string cell, std::string value) {
    //log the edit
    Edit edit;
    edit.cell = cell;
    edit.previous_value = get_cell(cell);
    edits.push(edit);

    if (value == "") {
        delete_value(cell);
    } else {
        set_value(cell, value);
    }

    increment_version();
}

std::string Spreadsheet::get_cell(std::string cell) {
    return get_value(cell);
}

/* Version incrementer and getter */
void Spreadsheet::increment_version() {
    version++;
}
int Spreadsheet::get_version() const {
    return version;
}

void Spreadsheet::
set_password(std::string password) {
    set_value(PASSWORD_KEY, password);
}

bool Spreadsheet::
passwords_match(std::string check) {
    string pass = get_value(PASSWORD_KEY);
    return pass == check;
}

/* Undo, returns the name of the cell that was affected.
 * Returns 'END' if there are no more edits to undo. */
std::string Spreadsheet::undo() {
    if(edits.empty())
        throw new no_more_undo();

    //change the cell and remove it from the stack
    std::string cell = edits.top().cell;
    /* delete the cell if it's empty */
    if (edits.top().previous_value == "") {
        delete_value(cell);
    } else {
        set_value(cell, edits.top().previous_value);
    }

    edits.pop(); //pop calls the Edit destructor

    //when we successfully undo, we also increment the version number
    increment_version();

    return cell;
}

Spreadsheet::Spreadsheet(std::string name) {
    version = 0;
    /* Add a .db to the end of the name */
    name = name + ".db";

    cout << "creating: " << name << endl;

    /* open the dptrbase */
    this->db = gdbm_open((char *) name.c_str(), 0, GDBM_WRCREAT, 0644, __gdbm_fatal_printer);
    if(!this->db) {
        printf("%s\n", gdbm_strerror(gdbm_errno));
        cout << "error opening db" << endl;
    }
}

void Spreadsheet::delete_value(string key) {
    datum dkey = {
        .dptr = (char *) key.c_str(),
        .dsize = strlen(key.c_str())
    };

    /* delete the key from the database */
    gdbm_delete(this->db, dkey);
}

void Spreadsheet::set_value(string key, string value) {
    datum dkey = {
        .dptr = (char *) key.c_str(),
        .dsize = strlen(key.c_str())
    };

    datum dvalue = {
        .dptr = (char *) value.c_str(),
        .dsize = strlen(value.c_str())
    };

    /* store the key replacing it if it exists */
    gdbm_store(this->db, dkey, dvalue, GDBM_REPLACE);
}

string Spreadsheet::get_value(string key) {

    datum dkey = {
        .dptr = (char *) key.c_str(),
        .dsize = strlen(key.c_str())
    };

    if (key == "") {
        return "";
    }

    datum value = gdbm_fetch(this->db, dkey);
    string return_val = string(value.dptr, value.dsize);

    /* we have to free the value's memory */
    free(value.dptr);

    return return_val;
}

string Spreadsheet::get_xml() {
    datum key = gdbm_firstkey(this->db);
    string val, strkey;

    ostringstream xml;

    xml << "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
    xml << "<spreadsheet>";

    /* while there's dptr */
    while(key.dptr != NULL) {
        strkey = string(key.dptr, key.dsize);
        /* skip the password key */
        if (strkey == PASSWORD_KEY) { 
            key = gdbm_nextkey(this->db, key);
            continue; 
        }
        val = this->get_value(strkey);
        /* escape the strings */
        escape_xml(strkey); escape_xml(val);

        xml << "<cell>";
        xml << "<name>" << strkey << "</name>";
        xml << "<contents>" << val << "</contents>";
        xml << "</cell>";
        key = gdbm_nextkey(this->db, key);
    }

    xml << "</spreadsheet>";
    return xml.str();
}

void Spreadsheet::clear_undo_stack() {
    while(!edits.empty()){
        edits.pop();
    }
}

Spreadsheet::~Spreadsheet() {
    cout << "Closing db" << endl;
    gdbm_close(this->db);
}
