#include <iostream>
#include <stdarg.h>
#include <sstream>
#include <stdlib.h>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <regex.h>
#include "Parser.h"
#include "Broadcast.h"
#include "Spreadsheet.h"
#include "SpreadsheetProvider.h"

using namespace std;

/* DEBUG DEFS */

#define DEBUG
//#define STRIP_CR

/* debugging routine. Works exactly like printf() */
void debug(const char * format, ...) {
#ifdef DEBUG
    va_list ap;
    va_start(ap, format);
    vfprintf(stderr, format, ap);
    va_end(ap);
#endif
}

/* get the length of a string that's utf-8 encoded */
unsigned long int utf8_length(const std::string &str) {
    const char * const string_bytes = str.c_str();
    size_t len = strlen(string_bytes), i = 0;
    unsigned long int chars = 0;
    while (i < len) {
        /* Start counting bytes */
        i += mblen(string_bytes + i, len - i);
        chars += 1;
    }
    return chars;
}

/* Expect some keys */
bool expected_keys(map<string,string> &data, int num_strings, string* keys){
    for(int i = 0; i < num_strings; i++){
        if (data.count(keys[i]) == 0) {
            return false;
        }
    }

    return true;

}

void Parser::
reset() {
    debug(">>> reset <<<\n");
    current_ctype = NO_CTYPE;
    line_data.clear();
    current_num_lines = 0;
    expected_num_lines = 0;
    change_err = NO_ERROR;
}

const string Parser::name_key = "NAME";
const string Parser::password_key = "PASSWORD";
const string Parser::version_key = "VERSION";
const string Parser::length_key = "LENGTH";
const string Parser::cell_key = "CELL";

Parser::
Parser() {
    populate();
}

void Parser::
tokenize_data(string line) {
    /* Make a new name */
    string name_part = line.substr(0, line.find(':'));
    string value_part = line.substr(line.find(':') + 1);
    /* upper-case the name */
    std::transform(name_part.begin(), name_part.end(), name_part.begin(), ::toupper);
    /* add this item to the line_data list */
    line_data[name_part] = value_part;
}

command_type Parser::
test_command_start(string line) {
    /* get the first part of the string */
    string word = line.substr(0, line.find(' '));
    std::transform(word.begin(), word.end(), word.begin(), ::toupper);

    if(word == "CREATE") {
        return CREATE;
    } else if (word == "JOIN") {
        return JOIN;
    } else if (word == "CHANGE") {
        return CHANGE;
    } else if (word == "UNDO") {
        return UNDO;
    } else if (word == "SAVE") {
        return SAVE;
    } else if (word == "LEAVE") {
        return LEAVE;
    } else {
        return NO_CTYPE;
    }
}

void Parser::
receive_raw(string raw) {
    stringstream update, response;
    SpreadsheetProvider &ss_prov = SpreadsheetProvider::getProvider();
    if (this->change_err == CELL_INVALID) {
        response << "CHANGE FAIL\n"
                 << "Name:" << this->line_data[name_key] << "\n"
                 << "Cell name invalid.\n";
    } else if (this->change_err == NO_SPREADSHEET) {
        response << "CHANGE FAIL\n"
                 << "Name:" << this->line_data[name_key] << "\n"
                 << "Spreadsheet does not exist." << "\n";
    } else if (this->change_err == NO_EDIT) {
        response << "CHANGE FAIL\n"
                 << "Name:" << this->line_data[name_key] << "\n"
                 << "Client has not yet joined the editing session." << "\n";
    } else if (this->change_err == WAIT) {
        Spreadsheet * ss = ss_prov.get_spreadsheet(this->line_data[name_key]);
        response << "CHANGE WAIT\n"
                 << "Name:" << this->line_data[name_key] << "\n"
                 << "Version:" << ss->get_version() << "\n";
    }

    /* if there's an error, return */
    if (this->change_err != NO_ERROR) {
        this->send_message(response.str());
        reset(); 
        /* Even though it's an error, we still need to catch the last newline */
        this->current_ctype = IGNORE;
        return;
    }

    /* uppercase the cell name */
    string cell = this->line_data[cell_key];
    std::transform(cell.begin(), cell.end(), cell.begin(), ::toupper);

    debug("Got Raw: %s\n", raw.c_str());
    debug("Name: %s\n", this->line_data[name_key].c_str());
    debug("Cell data: %s\n", cell.c_str());
    Spreadsheet * spreadsheet = ss_prov.get_spreadsheet(this->line_data[name_key]);
    /* Set the contents of the cell, distribute change to clients */
    spreadsheet->set_cell(cell, raw);
    Broadcast * broadcast = this->get_broadcast();
    /* generate the update message */
    update << "UPDATE\n"
           << "Name:" << this->line_data[name_key] << "\n"
           << "Version:" << spreadsheet->get_version() << "\n"
           << "Cell:" << cell << "\n"
           << "Length:" << this->line_data[length_key] << "\n"
           << raw << "\n";
    /* broadcast to all other editing clients excluding us */
    broadcast->broadcast(this->line_data[name_key], update.str(), this);

    response << "CHANGE OK\n"
             << "Name:" << this->line_data[name_key] << "\n"
             << "Version:" << spreadsheet->get_version() << "\n";
    this->send_message(response.str());
    /* we're done with this command */
    reset();

    /* Skip the next line, it should be empty */
    this->current_ctype = IGNORE;
}

void Parser::
receive_line(string line) {
#ifdef STRIP_CR
    /* remove the trailing carraige return  */
    if (line[line.length() - 1] == '\r') {
            line = line.substr(0, line.length() - 1);
    }
    //*/
#endif
    debug("Got Line: %s.\n", line.c_str());

    /* if we're to ignore the line, skip it */
    if (current_ctype == IGNORE) {
        reset();
        return;
    }

    // Test for command
    command_type line_ctype = test_command_start(line);

    // This is not a command line and we are midway
    // through buffering an existing command
    if(line_ctype == NO_CTYPE && current_ctype != NO_CTYPE){
        current_num_lines++;
        tokenize_data(line);

        //Are we ready to resolve the command?
        if (expected_num_lines <= current_num_lines) {
            debug("Resolving Command.\n");
            /* Call this command's callback */
            (this->*resolvers[current_ctype])();
            /* reset the state */
        }
    }

    // This is a command line
    // Flush the buffer then begin the parse for the new command
    else if(line_ctype != NO_CTYPE) {

        // We're not done with the previous
        // Send an error for incomplete previous command
        if(expected_num_lines != 0) {
            this->send_message("ERROR\n");
        }

        // Setup for new parse
        reset();
        current_ctype = line_ctype;
        current_num_lines = 1;

        // Set the # of lines we expect for this command
        if(line_ctype == CHANGE) {
            expected_num_lines = 5;
        }
        else if (line_ctype == LEAVE || line_ctype == SAVE){
            expected_num_lines = 2;
        }
        else{
            expected_num_lines = 3;
        }
    }
    else {
        this->send_message("ERROR\n");
    }
}

void Parser::
create() {
    // Command validation
    string keys[2];
    keys[0] = name_key;
    keys[1] = password_key;
    if(!expected_keys(line_data, 2, keys)) {
        this->send_message("ERROR\n");
        reset(); return;
    }

    stringstream output;
    SpreadsheetProvider &ss_prov = SpreadsheetProvider::getProvider();
    string name = line_data[name_key],
       password = line_data[password_key];

    // Check name exists:
    if(!ss_prov.exists(name)) {
        debug("NO Sheet.\n");
        // Create SS
        Spreadsheet *ss = ss_prov.get_spreadsheet(name);
        ss->set_password(password);
        // Send CREATE OK
        output << "CREATE OK\n"
               << "Name:" << name << "\n"
               << "Password:" << password << "\n";
    }
    else{
        output << "CREATE FAIL\n"
               << "Name:" << name << "\n"
               << "Name already exists.\n";
    }
    this->send_message(output.str());

    reset();
}

void Parser::
join() {
    // Command validation
    string keys[2];
    keys[0] = name_key;
    keys[1] = password_key;
    if(!expected_keys(line_data, 2, keys)) {
        this->send_message("ERROR\n");
        reset(); return;
    }

    string name = line_data[name_key],
           password = line_data[password_key];

    stringstream output;
    SpreadsheetProvider &ss_prov = SpreadsheetProvider::getProvider();
    Broadcast *broadcast = this->get_broadcast();
    // Spreadsheet does not exist:
    if(!ss_prov.exists(name)){
        // JOIN FAIL
        output << "JOIN FAIL\n"
               << "Name:" << name << "\n"
               << "Spreadsheet does not exist.\n";
    } else if (broadcast->is_subscribed(name, this)) {
        output << "JOIN FAIL\n"
               << "Name:" << name << "\n"
               << "Already editing.\n";
    } else {
        Spreadsheet *spreadsheet = ss_prov.get_spreadsheet(name);
        if (!spreadsheet->passwords_match(password)) {
            output << "JOIN FAIL\n"
                   << "Name:" << name << "\n"
                   << "Incorrect password.\n";
        } else {
            string xml = spreadsheet->get_xml();

            Broadcast * broadcast = this->get_broadcast();
            broadcast->subscribe(name, this);
            output << "JOIN OK\n"
                   << "Name:" << name << "\n"
                   << "Version:" << spreadsheet->get_version() << "\n"
                   << "Length:" << utf8_length(xml) << "\n"
                   << xml << "\n";
        }
    }

    //cout << output.str() << endl;

    this->send_message(output.str());
    reset();
}

void Parser::
undo() {
    // Command validation
    string keys[2];
    keys[0] = name_key;
    keys[1] = version_key;
    if(!expected_keys(line_data, 2, keys)) {
        this->send_message("ERROR\n");
        reset(); return;
    }
    // Check client is in edit session:
        // not: undo fail
    Broadcast * broadcast = this->get_broadcast();
    string name = line_data[name_key],
        version = line_data[version_key];
    stringstream output;


    int ss_version = atoi(version.c_str());
    Spreadsheet * ss = SpreadsheetProvider::getProvider().get_spreadsheet(name);

    // Check user editing
    if(!broadcast->is_subscribed(name, this)){
        output << "UNDO FAIL\n"
               << "Name:" << name << "\n";
    }
    // Check client up to date
    else if(ss_version != ss->get_version()){
        output << "UNDO WAIT\n"
               << "Name:" << name << "\n"
               << "Version:" << version << "\n";
    }
    else {
        try {
            string cell = ss->undo();
            string value = ss->get_cell(cell);
            stringstream update;
            update << "UPDATE\n"
                   << "Name:" << name << "\n"
                   << "Version:" << ss->get_version() << "\n"
                   << "Cell:" << cell << "\n"
                   << "Length:" << utf8_length(value.c_str()) << "\n"
                   << value << "\n";

            /* broadcast this change to all the other clients */
            Broadcast *broadcast = this->get_broadcast();
            broadcast->broadcast(name, update.str(), this);

            output << "UNDO OK\n"
                   << "Name:" << name << "\n"
                   << "Version:" << ss->get_version() << "\n"
                   << "Cell:" << cell << "\n"
                   << "Length:" << utf8_length(value.c_str()) << "\n"
                   << value << "\n";
        } catch(...) {
            // if there are no more undos, send a UNDO END
            output << "UNDO END\n"
                   << "Name:" << name << "\n"
                   << "Version:" << version << "\n";
        }
    }
    this->send_message(output.str());

    reset();
}
void Parser::
change() {
    // Command validation
    string keys[4];
    keys[0] = name_key;
    keys[1] = version_key;
    keys[2] = cell_key;
    keys[3] = length_key;
    if(!expected_keys(line_data, 4, keys)) {
        this->send_message("ERROR\n");
        reset(); return;
    }

    SpreadsheetProvider &ss_prov = SpreadsheetProvider::getProvider();
    Broadcast * broadcast = this->get_broadcast();

    /* uppercase the cell name */
    string cell = this->line_data[cell_key];
    std::transform(cell.begin(), cell.end(), cell.begin(), ::toupper);

    stringstream output;

    int version = atoi(this->line_data[version_key].c_str());
    /* get the said length */
    int length = atoi(this->line_data[length_key].c_str());
    /* set the mode of the LineClient to raw */
    this->set_raw((unsigned int) length);

    if (REG_NOMATCH == regexec(this->cell_matcher, cell.c_str(), 0, NULL, 0)) {
        this->change_err = CELL_INVALID;
        return; 
    /* Spreadsheet doesn't exists */
    } else if (! ss_prov.exists(this->line_data[name_key])) {
        this->change_err = NO_SPREADSHEET;
        return;
    /* If the user hasn't joined */
    } else if (! broadcast->is_subscribed(this->line_data[name_key], this)) {
        this->change_err = NO_EDIT;
        return;
    }
    /* The spreadsheet exists and we have access to it, go fetch it */
    Spreadsheet * spreadsheet = ss_prov.get_spreadsheet(this->line_data[name_key]);
    if (spreadsheet->get_version() != version) {
        this->change_err = NO_EDIT;
        return;
    }

    /* ok the change is valid, get the content */
}

void Parser::
save() {
    // Command validation
    string keys[1];
    keys[0] = name_key;
    if(!expected_keys(line_data, 1, keys)) {
        this->send_message("ERROR\n");
        reset(); return;
    }

    Broadcast * broadcast = this->get_broadcast();
    string name = line_data[name_key];
    stringstream output;

    // Check user editing
    if(!broadcast->is_subscribed(name, this)){
        output << "SAVE FAIL\n"
               << "Name:" << name << "\n"
               << "Not Subscribed.\n";
        this->send_message(output.str());
        reset();
        return;
    }

    // Reset undo stack
    Spreadsheet * ss = SpreadsheetProvider::getProvider().get_spreadsheet(name);
    ss->clear_undo_stack();

    output << "SAVE OK\n"
           << "Name:" << name << "\n";

    this->send_message(output.str());
    reset();
}
void Parser::
leave() {
    // Command validation
    string keys[1];
    keys[0] = name_key;
    if(!expected_keys(line_data, 1, keys)) {
        this->send_message("ERROR\n");
        reset(); return;
    }

    // All logic handled by Broadcast
    Broadcast * broadcast = this->get_broadcast();
    broadcast->unsubscribe(line_data[name_key], this);
    reset();
}

void Parser::
populate() {
    resolvers[CREATE]   = &Parser::create;
    resolvers[JOIN]     = &Parser::join;
    resolvers[CHANGE]   = &Parser::change;
    resolvers[UNDO]     = &Parser::undo;
    resolvers[SAVE]     = &Parser::save;
    resolvers[LEAVE]    = &Parser::leave;

    /* simple cell matcher */
    this->cell_matcher = (regex_t *) malloc(sizeof(regex_t));
    regcomp(this->cell_matcher, "[A-Z]+[0-9]+", REG_EXTENDED | REG_NOSUB);

    reset();
}

Parser::
~Parser() {
    /* free the regex */
    regfree(this->cell_matcher);
}
