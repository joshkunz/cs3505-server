#include <string>
#include <map>
#include <regex.h>
#include "LineClient.h"
#include "Spreadsheet.h"
#include "SpreadsheetProvider.h"

#ifndef PARSER_H
#define PARSER_H

enum command_type {
	CREATE,
	JOIN,
	CHANGE,
	UNDO,
	SAVE,
	LEAVE,
    IGNORE,     /* For handling arbitrary lines (like ones after raw messages) */
	NO_CTYPE	
};

enum change_error {
    CELL_INVALID,
    NO_SPREADSHEET,
    NO_EDIT,
    WAIT,
    NO_ERROR
};

class Parser : public virtual LineClient {
	typedef void (Parser::*resolver)();
	private:
	// Class members
		std::map<command_type,resolver> resolvers;
		std::map<std::string, std::string> line_data;
		
		// The type of the command presently being buffered, or 
		// command_type.NONE if the parser is cleared for a new command.
		command_type current_ctype;
		
		// The number of lines that have been sent so far in this command
		int current_num_lines;

		// The number of lines expected for this command type
		int expected_num_lines;

        /* matches cell names */
        regex_t * cell_matcher;

        // Used to manage sending ERROR message with raw bodies 
        change_error change_err;

	// Helper functions
		// These functions are invoked after the correct number 
		// of input lines have been buffered.
		// They have individual logic for the specific type of command
		void create();
		void join();
		void undo();
		void change();
		void save();
		void leave();

		// Reports the type of the input command, if any.
		// returns command_type.NONE if this does not match
		// the begining of a command
		command_type test_command_start(std::string line);

		// Invoked on a non-starting line to tokenize the input
		// Results will be stored in line_data
		void tokenize_data(std::string line);

		// Resets the state of the parser, clearing all input buffers
		void reset();

		// Fills class members with the appropriate
		// values. Invoked at the begining of the object lifecycle.
		void populate();

		// replacement for "magic strings"
		static const std::string name_key;
		static const std::string password_key;
		static const std::string version_key;
		static const std::string length_key;
		static const std::string cell_key;
		static const std::string content_key;


	public:
        ~Parser();
		Parser();
		void receive_line(std::string s);
		void receive_raw(std::string s);
};
#endif
