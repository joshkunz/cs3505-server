#include <string>
#include <map>
#include <iostream>
#include <sys/stat.h>
#include "Spreadsheet.h"
#include "SpreadsheetProvider.h"

/* Get a spreadsheet, whether it already exists or not */
Spreadsheet* SpreadsheetProvider::get_spreadsheet(std::string name) {
    if(obj_exists(name))
        return spreadsheets[name];

    // Spreadsheet does not yet exist, so create it
    spreadsheets[name] = new Spreadsheet(name);

    return spreadsheets[name];
}

bool SpreadsheetProvider::obj_exists(std::string name) const {
    return spreadsheets.count(name) > 0;
}


/* Check if the spreadsheet with name 'name' exists */
bool SpreadsheetProvider::exists(std::string name) const {
    struct stat out;
    if (this->obj_exists(name) || (!stat((name + ".db").c_str(), &out))) {
        return true;
    } else {
        return false;
    }
}

SpreadsheetProvider &SpreadsheetProvider::getProvider() {
    static SpreadsheetProvider provider;
    return provider;
}

SpreadsheetProvider::~SpreadsheetProvider() {
    for(std::map<std::string, Spreadsheet*>::iterator i=spreadsheets.begin(); i!=spreadsheets.end(); ++i)
        delete i->second;
    spreadsheets.clear();
}
