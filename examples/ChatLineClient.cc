#include <string>
#include <cstdio>
#include <cstdlib>
#include "Server.h"
#include "Broadcast.h"
#include "ChatLineClient.h"

ChatLineClient::
ChatLineClient()
    : name("Anon"), channel(""), connected(false) {}

void ChatLineClient::
send_help() {
    std::string help = 
    "Welcome to the chat server!\n"
    "\n"
    "Commands:\n"
    "   help - print this help message.\n"
    "   name <name> - change your name to <name>.\n"
    "   join <channel> - join the channel <channel>.\n"
    "   opus <chars> - Send a long-form message of length <chars>\n"
    "   <text> - send the text on your current channel.\n"
    ;
    /* send the help message */
    this->send_message(help);
}

void ChatLineClient::
receive_raw(std::string raw) {
    Broadcast *b = this->get_broadcast();
    if (!connected) { return; }
    /* broadcast the opus */
    b->broadcast(this->channel, "<<" + this->name + ">>\n" + raw + '\n');
}

void ChatLineClient::
receive_line(std::string line) {
    printf("Got line!: %s.\n", line.c_str());
    /* remove the trailing carraige return */
    if (line[line.length() -1] == '\r') {
        line = line.substr(0, line.length() - 1);
    }
    /* find the first space */
    size_t first_space = line.find(' ', 0);
    std::string first_part,
                second_part;
    Broadcast * broadcast = this->get_broadcast();
    /* Get the line parts */
    if (first_space == std::string::npos) {
        first_part = "";
        second_part = line;
    } else {
        first_part = line.substr(0, first_space);
        /* don't include the space in the second part */
        second_part = line.substr(first_space + 1);
    }

    /* display the help text */
    if (second_part == "help") {
        this->send_help();
    /* send a long-form message of second_part characters */
    } else if (first_part == "opus") {
        /* say we want 'second_part' characters raw */
        this->set_raw(atoi(second_part.c_str()));
    /* change your name */
    } else if (first_part == "name") {
        this->name = second_part;
        this->send_message("Name set to: " + this->name + "\n");
    /* change the channel */
    } else if (first_part == "join") {
        if (this->connected) {
            broadcast->unsubscribe(this->channel, this);
        }
        this->channel = second_part;
        broadcast->subscribe(this->channel, this);
        this->connected = true;
        this->send_message("Joined channel: " + second_part + "\n");
    /* Otherwise, broadcast the message */
    } else {
        if (! this->connected) { return; }
        broadcast->broadcast(this->channel, 
        /* send the build message */
        this->name + ": " + first_part + " " + second_part + "\n",
        this);
    }
}
