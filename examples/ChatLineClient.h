#include <string>
#include "LineClient.h"

#ifndef CHAT_LINE_CLIENT_H
#define CHAT_LINE_CLIENT_H

class ChatLineClient : public virtual LineClient {
    private:
        std::string name;
        std::string channel;
        bool connected;

        void send_help();
    public:
        ChatLineClient();
        void receive_line(std::string line);
        void receive_raw(std::string raw);
};

#endif 
