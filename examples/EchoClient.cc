#include <sys/types.h>
#include <sys/socket.h>
#include <cstring>
#include <cstdio>
#include "EchoClient.h"

/* read 1Kib off the socket */
#define BUFFSIZE 1024

EchoClient::
EchoClient()
    : socket(-1) {}

void EchoClient::set_socket(int socket) {
    this->socket = socket;
}

int EchoClient::
run() {
    while (true) {
        char data[BUFFSIZE];
        /* Read some data off the socket */
        int got_bytes = recv(this->socket, data, BUFFSIZE, 0);
        /* if the client closes the connection, exit */
        if (got_bytes == 0) {
            return 0;
        }
        /* upper-case the bytes */
        for (int i = 0; i < got_bytes; i++) {
            if ( data[i] >= 'a' && data[i] <= 'z') {
                data[i] += ('A' - 'a');
            }
        }

        int sent_bytes = 0;
        /* send all of the upper-cased data */
        while (sent_bytes < got_bytes) {
            sent_bytes += send(this->socket, data + sent_bytes,
                               got_bytes - sent_bytes, 0);
        }
        /* blank out the buffer */
        memset(data, 0, got_bytes);
    }
}
