#include "Server.h"

#ifndef ECHO_CLIENT_H
#define ECHO_CLIENT_H

class EchoClient : public virtual Client {
    private:
        int socket;
    public:
        EchoClient();
        void set_socket(int socket);
        int run();
        ~EchoClient() {}
};

#endif
