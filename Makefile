.PHONY: clean default zip grind gdb

CXX = clang++
CXXFLAGS = -g -O0 -Wall -Wextra -Werror
LDFLAGS = -g
LDLIBS = -lpthread -lgdbm

zipfiles = Makefile *.cc *.h tools examples tests
zipfile = server.zip

default: server

#client_objects = ChatLineClient.o LineClient.o
client_objects = Parser.o LineClient.o

# Clients
EchoClient.o: Server.h
LineClient.o: Server.h
EchoLineClient.o: LineClient.h
ChatLineClient.o: LineClient.h
Parser.o: LineClient.h

# Spreadsheets
Spreadsheet.o:
SpreadsheetProvider.o: Spreadsheet.h
spreadsheet_tester = tests/testspreadsheet.cc

# Server
main.o: 
Broadcast.o:
Server.o: Broadcast.h

spreadsheet_objects = Spreadsheet.o SpreadsheetProvider.o
server_objects = Server.o Broadcast.o

testers = testSpreadsheet

full_objects = $(spreadsheet_objects) $(server_objects) $(client_objects) main.o

client_headers = $(pathsubst %.o,%.h,$(active_client))


server: $(full_objects)
	$(CXX) $(LDFLAGS) -o server $(full_objects) $(LDLIBS)

# compile only the client files 
client: $(filter-out main.o,$(full_objects))

gdb: server
	gdb ./server

grind: server
	valgrind ./server

test: $(testers) 
	./testSpreadsheet

testSpreadsheet: $(spreadsheet_objects)
	$(CXX) $(LDFLAGS) -o testSpreadsheet $(CXXFLAGS) $(spreadsheet_tester) $(spreadsheet_objects) $(LDLIBS)

zip: $(zipfiles)
	zip -r $(zipfile) $(zipfiles)

clean:
	-rm server *.gch *.db $(full_objects) $(testers) $(zipfile)

telnet:
	python27 tools/linenet.py localhost 5000
