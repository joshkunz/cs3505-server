import socket
import threading
from time import sleep

LINE_END = u"\r\n"

conn = socket.create_connection(("localhost", 5000))
conn.sendall(LINE_END.join("join #", "name bob\n"))
i = 0
while True:
    conn.sendall("hi {0}\n".format(i))
    i += 1

def print_thread():
    data = conn.recv(1024)
    print data

threading.Thread(target=print_thread).start()


