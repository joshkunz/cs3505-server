# send a unicode opus to the chat-line clinet.

import socket
import random
from time import sleep

L = u"\r\n"
OPUS_LEN = (500, 1000)

cnxn = socket.create_connection(("localhost", 5000))

# connect to the channel
cnxn.sendall(L.join(("join #", "name testbot")) + L)

length = random.randrange(*OPUS_LEN)

#generate our random string
rand_unicode = u"".join([unichr(random.randrange(1, 0xffff)) for x \
                        in range(length)])

# send the opus command
cnxn.sendall("opus {0}{1}".format(length, L))
# send the unicode
cnxn.sendall(rand_unicode.encode("utf-8"))
cnxn.sendall("Hello friend" + L)

sleep(1)
cnxn.close()
