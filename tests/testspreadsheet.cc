#include <iostream>
#include "../SpreadsheetProvider.h"

using namespace std;

int main() {
    cout << "Testing Spreadsheet" << endl;

    // test that a spreadsheet can be retrieved
    bool before =  SpreadsheetProvider::getProvider().exists("test");
    Spreadsheet *s = SpreadsheetProvider::getProvider().get_spreadsheet("test");

    // only returns a value after the first run, test for persistance
    cout << "Persistance check: " << s->get_cell("K7") << endl;

    bool after = SpreadsheetProvider::getProvider().exists("test");
    if (before == after)
        cout << "Test failed! Spreadsheet failed to be created" << endl;

    // check that getting the spreadsheet again returns the same spreadsheet
    Spreadsheet *r = SpreadsheetProvider::getProvider().get_spreadsheet("test");
    if (s != r)
        cout << "Test failed! Two Spreadsheet objects created for same spreadsheet" << endl;

    // make sure that they are working on the same data
    s->increment_version();
    int version1 = s->get_version();
    if(version1 != r->get_version())
        cout << "Test failed! Two Spreadsheet objects have different version numbers for same spreadsheet" << endl;

    // check that a cell can be edited
    s->set_cell("A1", "12");

    if(s->get_cell("A1") != "12")
        cout << "Test failed! Setting a cell did not change its value" << endl;

    // test iteration and xml
    cout << "xml test:\n" << s->get_xml();

    // check that undo works properly
    string cell = s->undo();
    if(cell != "A1")
        cout << "Test failed! Undo did not change the correct cell" << endl;

    if(s->get_cell(cell) != "")
        cout << "Test failed! Undo did not change the cell value" << endl;

    if(s->get_version() <= version1)
        cout << "Test failed! Undo did not change the version number" << endl;

    bool thrown = false;
    try {
        s->undo();
    }
    catch(...) { thrown = true;}

    if(!thrown)
        cout << "Test failed! Undo did not report that there are no more undos" << endl;

    // quick speed test to see how performant the spreadsheet is
    for(int i=0; i<10000; i++) {
        s->set_cell("A2", "=A1+A1");
    }

    string cellundo;
    for(int i=0; i<500; i++) {
        cellundo = s->undo();
    }

    if(cellundo != "A2")
        cout << "Test failed! Multiple undos did not end up correct" << endl;

    // test for persistance
    s->set_cell("K7", "check");

    cout << "Finished Testing Spreadsheet" << endl;
}
