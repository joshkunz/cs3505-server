#include <string>
#include <map>
#include "Spreadsheet.h"

#ifndef SPREADSHEET_PROVIDER_H
#define SPREADSHEET_PROVIDER_H

/* A class that creates Spreadsheet objects,
 * and ensures only one such object exists for each spreadsheet.
 * This provider itself is a singleton. */
class SpreadsheetProvider {
    private:
        /* Maintain a map of spreadsheets, keyed by the spreadsheet name */
        std::map<std::string, Spreadsheet*> spreadsheets;

        /* override copy constructors and constructor to prevent copying */
        SpreadsheetProvider(SpreadsheetProvider const&);
        void operator=(SpreadsheetProvider const&);
        SpreadsheetProvider(){};
        /* check if there's an object for the spreadsheet with 'name' */
        bool obj_exists(std::string name) const;

    public:
        /* Get a spreadsheet, whether it already exists or not */
        Spreadsheet *get_spreadsheet(std::string name);

        /* Check if the spreadsheet with name 'name' exists */
        bool exists(std::string name) const;

        /* Gets the instance of the provider */
        static SpreadsheetProvider& getProvider();
        ~SpreadsheetProvider(); // destructor
};

#endif
